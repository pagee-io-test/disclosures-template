# Wize Pages - Home's Value

## Installation

~~~bash
    # System Deps
    brew install ruby-build rbenv
    gem install bundler jekyll
    # Ruby Dep
    rbenv install 2.6.3
    rbenv local 2.6.3
    rbenv rehash
    # App deps
    bundle install
    npm install
~~~

## Running the development server

~~~bash
    # Jekyll's test server for compiling files.
    bundle exec jekyll serve
    # Gulp is used to compile static content.
    gulp
~~~

## Deploy

~~~bash
    # Commit YOUR changes
    git commit -m"Change Message"
    # Push the changes to the pages branch to build and deploy
    git push origin pages
~~~

### Gitlab Jekyll Page Creation Process

1. Fork this repository: https://gitlab.com/wizepages/disclosures/-/forks/new
2. Ensure that there is a gitlab-ci CI/CD file (should need to do anything to this)
3. Create a branch called pages (if one does not already exist)
4. Run the CI/CD Job (or commit changes and push to the pages branch to trigger auto running the CI/CD job)
