let card1 = $('.card1');
let card2 = $('.card2');

$('form').submit(function(event) {
  event.preventDefault();
  if ($('form').valid() === true) {
    card1.fadeOut(function() {
      card2.fadeIn();
    });
    $.post($('form').attr('action'));
  }
});
