const concat   = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const del      = require('del');
const gulp     = require('gulp');
const favicons = require('gulp-favicons');
const imagemin = require('gulp-imagemin');
const prefix   = require('gulp-autoprefixer');
const sass     = require('gulp-sass');
const terser   = require('gulp-terser');
const jslint   = require('gulp-jslint');
const cp       = require('child_process');

const argv = require('yargs').argv;
const isProduction = (argv.production === undefined) ? false : true;

let config   = require('./package.json');


if (isProduction === true) {
  config.dest = 'public';
}

/**
 * * Clears the entire build directory from the distribution folder.
 *
 * @task {jekyll:serve}
 */
gulp.task('clean:all', function(done) {
  del(config.dest);
  done();
});


/**
 * Tasks for building/copying images.
 *
 * @task {images:build}
 */
gulp.task('images:build', function(done) {
  gulp.src(config.images)
    .pipe(imagemin())
    .pipe(gulp.dest(config.dest + '/assets/imgs'))
  done();
});


/**
 * Tasks for clearing images from the distribution folder.
 *
 * @task {images:clean}
 */
gulp.task('images:clean', function(done) {
  del(config.dest + '/assets/imgs');
  done();
});


/**
 * Tasks for building/copying fonts.
 *
 * @task {fonts:build}
 */
gulp.task('fonts:build', function(done) {
  gulp.src(config.fonts)
   .pipe(gulp.dest(config.dest + '/assets/fonts'))
  done();
});


/**
 * Tasks for clearing fonts from the distribution folder.
 *
 * @task {fonts:clean}
 */
gulp.task('fonts:clean', function(done) {
  del(config.dest + '/assets/fonts');
  done();
});


/**
 * Tasks for clearing JavaScript from the distribution folder.
 *
 * @task {js:clean}
 */
gulp.task('js:clean', function(done) {
  del(config.dest + '/assets/js');
  done();
});

gulp.task('js:lint', function (done) {
  for (item in config.js) {
    if (item == undefined) {break;}
    gulp.src(config.js[item])
      .pipe(jslint())
      .pipe(jslint.reporter('default'))
  }
  done();
});


/**
 * Tasks for compiling production JavaScript.
 *
 * @task {js:compile}
 */
gulp.task('js:compile', function(done) {
  for (item in config.js) {
    if (item == undefined) {break;}
    gulp.src(config.js[item])
      .pipe(concat(item + '.js'))
      .pipe(terser().on('error', console.error))
      .pipe(gulp.dest(config.dest + '/assets/js'))
  }
  done();
});


/**
 * Tasks for building development JavaScript.
 *
 * @task {js:pretty}
 */
gulp.task('js:pretty', function(done) {
  for (item in config.js) {
    if (item == undefined) {break;}
      gulp.src(config.js[item])
      .pipe(concat(item + '.js'))
      .pipe(gulp.dest(config.dest + '/assets/js'))
  }
  done();
});


/**
 * Tasks for clearing CSS.
 *
 * @task {css:clean}
 */
gulp.task('css:clean', function(done) {
  del(config.dest + '/css');
  done();
});


/**
 * Tasks for building CSS production ready.
 *
 * @task {css:compile}
 */
gulp.task('css:compile', function(done) {
  for (item in config.css) {
    if (item == undefined) {break;}
    gulp.src(config.css[item])
      .pipe(sass({
        errLogToConsole: true
      }))
      .pipe(concat(item + '.css'))
      .pipe(cleanCSS())
      .pipe(prefix('last 3 versions', '> 1%', 'ie 8'))
      .pipe(gulp.dest(config.dest + '/assets/css'));
  }
  done();
});


/**
 * Tasks for building CSS development.
 *
 * @task {css:pretty}
 */
gulp.task('css:pretty', function(done) {
  var paths = [];
  for (item in config.css) {
    if (item == undefined) {break;}
    paths.push.apply(paths, config.css[item])
  }
  gulp.src(paths)
    .pipe(sass({
      errLogToConsole: true
    }))
    .pipe(prefix('last 3 versions', '> 1%', 'ie 8'))
    .pipe(gulp.dest(config.dest + '/assets/css'));
  done();
});


/**
 * Tasks for building favicons.
 *
 * @task {favicon:build}
 */
gulp.task('favicon:build', function(done) {
  gulp.src(config.favicon)
    .pipe(favicons({
      appName: config.name? config.name : '',
      appShortName: config.shortName ? config.shortName : config.name,
      appDescription: config.description ? config.description : '',
      developerName: config.author.name ? config.author.name : '',
      developerURL: config.author.url ? config.author.url : '',
      background: '#FFFFFF',
      path: '/assets/favicons',
      url: config.url,
      display: 'standalone',
      orientation: 'portrait',
      scope: '/',
      start_url: '/',
      version: 1.0,
      html: 'favicons.html',
      logging: false,
      pipeHTML: true,
      replace: true,
    })
  )
  .pipe(gulp.dest(config.dest + '/assets/favicons'));
  done();
})


/**
 * Tasks for clearing Favicons.
 *
 * @task {favicon:clean}
 */
gulp.task('favicon:clean', function(done) {
  del(config.dest + '/assets/favicons');
  done();
});


/**
 * Tasks for watching over tasks that may change during development
 * and re-run a command(s) to compile the changes.
 *
 * @task {watch}
 */
gulp.task('watch', function(done) {
  gulp.watch(config.watch.css, gulp.series('css:pretty'));
  gulp.watch(config.watch.js, gulp.series('js:pretty'));
  gulp.watch(config.watch.fonts, gulp.series('fonts:build'));
  gulp.watch(config.watch.images, gulp.series('images:build'));
  done();
});


/**
 * The common task commands to be used.
 */
let build = ['fonts:build', 'images:build', 'favicon:build'];
let clean = [
  'fonts:clean', 'images:clean', 'favicon:clean',
  'js:clean', 'css:clean',
];

gulp.task('build',   gulp.series(build));
gulp.task('clean',   gulp.series(clean));
gulp.task('compile', gulp.series('build', 'js:compile', 'css:compile'));
gulp.task('pretty',  gulp.series('build', 'js:pretty', 'css:pretty'));
gulp.task('default', gulp.series('pretty', 'watch'));
